<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Category;
use Illuminate\Database\Seeder;

class addRecordsToCategoriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert(array([
            'name' => 'electronic devices',
            'created_at' => Carbon::now()
        ],[
            'name' => 'Appliances',
            'created_at' => Carbon::now()
        ]));
        $elec = Category::where('name','electronic devices')->first()->id;
        Category::insert(array([
            'name' => 'mobile',
            'parent_id'=>$elec,
            'created_at' => Carbon::now()
        ],[
            'name' => 'houseHolds',
            'paent_id'=>$elec,
            'created_at' => Carbon::now()
        ]));
        $mob = Category::where('name','mobile')->first()->id;
        Category::insert([
            'name' => 'samsung',
            'parent_id'=>$mob,
            'created_at' => Carbon::now()
        ]);
    }
}
