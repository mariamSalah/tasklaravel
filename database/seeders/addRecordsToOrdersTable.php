<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\ShopProduct;
use Illuminate\Database\Seeder;

class addRecordsToOrdersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shops = ShopProduct::pluck('id');
        foreach($shops as $shop)
        {
            Order::insert([
                'shop_product_id' => $shop,
                'created_at' => Carbon::now()
            ]);
        }

    }
}
