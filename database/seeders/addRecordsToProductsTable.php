<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Seeder;

class addRecordsToProductsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elec = Category::where('name','electronic devices')->first()->id;
        $samsung = Category::where('name','samsung')->first()->id;
        $mob = Category::where('name','mobile')->first()->id;
        Product::insert(array([
            'name' => 'tv',
            'category_id' => $elec,
            'price' => 15666,
            'created_at' => Carbon::now()
        ],[
            'name' => 'samsung galaxy s2',
            'category_id' => $samsung,
            'price' => 15666,
            'created_at' => Carbon::now()
        ],[
            'name' => 'iphoneX',
            'category_id' => $mob,
            'price' => 15666,
            'created_at' => Carbon::now()
        ]));
    }
}
