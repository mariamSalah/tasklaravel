<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Product;
use App\Models\ShopProduct;
use Illuminate\Database\Seeder;

class addRecordsToShopProductsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tv = Product::where('name','tv')->first()->id;
        $iphone = Product::where('name','iphoneX')->first()->id;
        ShopProduct::insert(array([
            'product_id'=>$tv,
            'created_at' => Carbon::now()
        ],[
            'product_id'=>$iphone,
            'created_at' => Carbon::now()
        ]));
    }
}
