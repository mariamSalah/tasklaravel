<?php

use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('main', [MainController::class, 'index'])->name('main');
Route::get('order', [MainController::class, 'order'])->name('order');
Route::get('latest', [MainController::class, 'getRecentProducts'])->name('latest');
