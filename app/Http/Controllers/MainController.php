<?php

namespace App\Http\Controllers;

use stdClass;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use App\Mail\MailMailable;
use App\Models\ShopProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;


class MainController extends Controller
{

    public function index( Request $request)
    {
        $category = Category :: find( $request->id );
        if(!$category)
        {
            return Redirect::back()->with('message','Please Enter a Valid ID to Proceed');

        }
        $allChildren = $category->allChildren();
        $allCategories = $allChildren->all();

        array_push($allCategories,$category);
        $shopProducts = [];
        foreach($allCategories as $category)
        {
            $shopProduct = $category->shopProducts()->get()->all();
            if(!empty($shopProduct))
            $shopProducts = array_merge($shopProducts , $shopProduct);
        }
        return view('show',["shopproducts" => $shopProducts, "subcategories" => $allChildren]);
    }

    public function order(Request $request){
        $shopProduct = ShopProduct::find($request->id);
        if(!$shopProduct)
        {
            return Redirect::back()->with('error','Please Enter a Valid ID to Proceed');
        }
            Order::insert([
                'shop_product_id' => $request->id,
                'created_at' => Carbon::now()

            ]);
        $order = Order::where('shop_product_id',$request->id)->first();
        $product = Product::find($shopProduct->first()->product_id)->first();

        Mail::raw('An order of id '.$order->id.' has been placed for product '.$product->name." at : ".$order->created_at." with total price : ".$product->price."EGP", function($message)
        {

            $message->from('mariamsalaheldin16@gmail.com')->to('shaimaa@mv-is.com')->subject('New Order');
        });
        return back()->with('success','Order Has Placed Successfully, Please Check Your E-Mail');
    }
    public function getRecentProducts(){
        $categories = Category::all();
        $products = [];

        foreach($categories as $c)
        {
            $prod = $c->shopProducts()->orderBy('created_at','DESC')->first();
            if($prod)
            {
                $product = new \StdClass;
                $product->id = $prod->id;
                $product->name = $prod->product->name;
                $product->category = $c->name;
                $product->created_at = $prod->created_at;
                array_push($products , $product);
            }


        }
        return view('latestProducts',['products'=>$products]);
    }
}
