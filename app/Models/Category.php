<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class Category extends Model
{
    use HasFactory;
    protected $fillable=[
        'name',
        'parent_id'
    ];
    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
    public function shopProducts(): HasOneThrough
    {
        return $this->hasOneThrough(ShopProduct::class, Product::class);
    }
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id' , 'id')->with('children');
    }
    public function allChildren()
    {
        $children = $this->children()->get();
        if(empty($children))
        {
            return $children;
        }
        foreach($children as $child)
        {
            $children = $children->merge($child->allChildren());
        }
        return $children;

    }
}
