<!DOCTYPE html>
<body>
    <div>
        @if($subcategories->isEmpty())
            <strong><u> No Child Categories </u></strong><br>
        @else
            <strong><u> Child Categories </u></strong><br><br>
            @foreach ($subcategories as $subcategory)
                child id = {{ $subcategory->id }}<br>
                child name = {{ $subcategory->name }}<br>
                <hr>
            @endforeach
        @endif
        <hr>
        <hr>
        <hr>
        @if(empty($shopproducts))
            No Shop Products For this Category<br>
        @else
            <strong><u> Shop Products </u></strong><br><br>
            @foreach ($shopproducts as $shopproduct)
                Shop product id = {{ $shopproduct->id }}<br>
                Shop product name = {{ $shopproduct->product->name }}<br>
                <hr>
            @endforeach
        @endif
    </div>
</body>
