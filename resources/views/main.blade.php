
<body>
<title> Welcome To my Little E-Commerce Site </title>

    @if (session('error'))
        <div class="alert alert-danger">
            <strong style="color:red; text-align:center"> {{ session('error') }} </strong>
        </div>
    @elseif (session('success'))
        <div class="alert alert-success">
            <strong style="color:green; text-align:center"> {{ session('success') }} </strong>
        </div>
    @endif
    <div>
        <form action="{{ route('order')}} " method="GET">
            <label> Make Order </label><br>
            <label> enter shop product id </label>
            <input type = "number" required id="id" name="id">
            @error('id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <input type= "submit" value="make order">

        </form>


    </div>
    <div>
        <form action=" {{ route('main')}} " method="GET">
            <label> show children </label><br>
            <label>please enter category id</label>
            <input type="number" required id="id" name="id">
            <input type= "submit" value="show">
            @error('id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </form>
    </div>
    <div>
        <form action="{{ route('latest') }}" method="GET">
            <button type = "submit" >get latest shop products</button>
        </form>
    </div>
</body>
